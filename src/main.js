import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'

require('./store/subscribe')

//CSS
import './assets/styles/_variables.scss'
import 'bootstrap/dist/css/bootstrap.min.css'
import './assets/styles/_side-bar.scss'
import './assets/styles/_sidebar-themes.scss'
import './assets/styles/_custom.scss'
import './assets/styles/_common.scss'
import './assets/styles/_autocomplete.scss'
import './assets/styles/_side-header-switch.scss'
import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css'

axios.defaults.baseURL = 'http://localhost/mmri-web-service/public/api/'
Vue.config.productionTip = false

store.dispatch('auth/attempt',localStorage.getItem('token')).then(() => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})

