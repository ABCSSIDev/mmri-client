import Vue from 'vue'
import VueRouter from 'vue-router'
import AcctDashboard from '../views/accounting/Dashboard'
import Items from '../views/accounting/Item'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    redirect: '/accounting'
  },
  {
    path: '/accounting',
    name: 'Dashboard',
    component: AcctDashboard,
    children: [
      {
        path: 'items',
        name: 'Items',
        component: Items
      }
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
